package br.com.caiosaderio.bebeugua;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity {
    public static MainActivity instace = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        instace = this;

        //TEMPO EM MILISEGUNDOS
        Long tempoProximoCopo = System.currentTimeMillis() + (1800*1000);
//        Long tempoProximoCopo = System.currentTimeMillis() + (10*1000);
        //INTENT PARA IR DEPOIS DA PENDENCIA
        Intent chamar = new Intent(this,BeberAgua.class);
        //PENDENCIA
        PendingIntent pendingIntent = PendingIntent.getActivity(this,1,chamar,0);
        //CONTROLE DE TEMPO
        AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, tempoProximoCopo, pendingIntent);
    }
}
